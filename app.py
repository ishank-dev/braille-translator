# Translate alphabet based text to braille.
from googletrans import Translator
from flask import Flask,request,jsonify
from languages import supported_languages
import mapAlphaToBraille, mapBrailleToAlpha,alphaToBraille,brailleToAlpha
from alphaToBraille import translate_to_braille
from brailleToAlpha import translate_to_text
app = Flask(__name__) 
translator = Translator()
results = {'text' : '', 'src' : '', 'dest' : ''}


# GET request for the landing page
@app.route('/', methods=['GET'])
def test():
	return jsonify({'message' : 'Welcome to Braille Translator! Refer documentation for more details',
                 'documentation' : 'https://gist.github.com/ishank-dev/747b5e4ed2edcabb57ca39db83b77f8a'})

# GET request to display all the languages with their respective codes
@app.route('/languages', methods=['GET'])
def returnAll():
    return jsonify({'available languages' : supported_languages})
'''
POST request
text = "Text to be translated"
dest = "Destination in which the language needs to be translated
Use code = 'brl' for braille translation"
For translation in other language visit /languages as a GET request
'''
@app.route('/translate', methods=['POST'])
def addOne():
    text = request.form['text']
    dest = request.form['dest']
    if  dest == 'brl':
        src = translator.detect(text).lang
        string = translator.translate(request.form['text'],src=src,dest='en').text
        braille = translate_to_braille(string)
        results = {'text':text,'src':src,'dest':dest,'braille_text':braille}

        return jsonify({'results' : results}) 

    else:
        src = translator.detect(text).lang
        results = {'text' : text, 'src' : src, 'dest' : dest}
        translate = translator.translate(text, src=src, dest=dest)
        translated = translate.text
        results['translated'] = translated
        print(results)
        return jsonify({'result' : results})

if __name__ == '__main__':
	app.run(debug=True, port=8080) #run app on port 8080 in debug mode


